Assets =
{
    Asset("ANIM", "anim/nigthmarephaseindicator.zip"),
}

local _G = GLOBAL
local require = _G.require

local visible_animation      = GetModConfigData("VISIBLE_ANIMATION")
local timer                  = GetModConfigData("TIMER")
local phase_name             = GetModConfigData("PHASE_NAME")
local horizontal_alignment   = GetModConfigData("HORIZONTAL_ALIGNMENT")
local vertical_alignment     = GetModConfigData("VERTICAL_ALIGNMENT")
local horizontal_margin      = GetModConfigData("HORIZONTAL_MARGIN")
local vertical_margin        = GetModConfigData("VERTICAL_MARGIN")
local scale                  = GetModConfigData("SCALE")
local visibly_have_medallion = GetModConfigData("VISIBLY_HAVE_MEDALLION")

_G.STRINGS.NIGHMARE_PHASE_INDICATOR =
{
    PHASENAMES = 
    {
        calm = "Phase calm",
        warn = "Phase warn",
        nightmare = "Phase wild",
        dawn = "Phase dawn",
    }
}

local Widget                  = require "widgets/widget"
local NightmarePhaseIndicator = require "widgets/nightmarephaseindicator"

local function AddNightmarePhaseIndicator(self)
    if _G.GetNightmareClock() ~= nil then
        -- normalize position in logic coords game, default center screen
        local position = {x = 0, y = 0, hanchor = _G.ANCHOR_MIDDLE, vanchor = _G.ANCHOR_MIDDLE}

        if horizontal_alignment == "LEFT" then
            position.hanchor = _G.ANCHOR_LEFT
            position.x = horizontal_margin
        elseif horizontal_alignment == "RIGHT" then
            position.hanchor = _G.ANCHOR_RIGHT
            position.x = -horizontal_margin
        end

        if vertical_alignment == "TOP" then
            position.vanchor = _G.ANCHOR_TOP
            position.y = -vertical_margin
        elseif vertical_alignment == "BOTTOM" then
            position.vanchor = _G.ANCHOR_BOTTOM
            position.y = vertical_margin
        end

        self.nightmarephaseindicator = self.under_root:AddChild(Widget("nightmarephaseindicator_root"))
        self.nightmarephaseindicator:SetScaleMode(_G.SCALEMODE_PROPORTIONAL)
        self.nightmarephaseindicator:SetHAnchor(position.hanchor)
        self.nightmarephaseindicator:SetVAnchor(position.vanchor)
        self.nightmarephaseindicator:SetPosition(position.x, position.y)
        self.nightmarephaseindicator = self.nightmarephaseindicator:AddChild(NightmarePhaseIndicator())

        if visible_animation then
            self.nightmarephaseindicator:AnimationOn()
        else
            self.nightmarephaseindicator:AnimationOff()
        end

        if timer == "TOP" then
            self.nightmarephaseindicator:SetPositionTimerTop()
        elseif timer == "RIGHT" then
            self.nightmarephaseindicator:SetPositionTimerRight()
        elseif timer == "BOTTOM" then
            self.nightmarephaseindicator:SetPositionTimerBottom()
        elseif timer == "LEFT" then
            self.nightmarephaseindicator:SetPositionTimerLeft()
        elseif timer == "CENTER" then
            self.nightmarephaseindicator:SetPositionTimerCenter()
        else
            self.nightmarephaseindicator:HideTimer()
        end

        if phase_name == "TOP" then
            self.nightmarephaseindicator:SetPositionPhaseNameTop()
        elseif phase_name == "RIGHT" then
            self.nightmarephaseindicator:SetPositionPhaseNameRight()
        elseif phase_name == "BOTTOM" then
            self.nightmarephaseindicator:SetPositionPhaseNameBottom()
        elseif phase_name == "LEFT" then
            self.nightmarephaseindicator:SetPositionPhaseNameLeft()
        elseif phase_name == "CENTER" then
            self.nightmarephaseindicator:SetPositionPhaseNameCenter()
        else
            self.nightmarephaseindicator:HidePhaseName()
        end

        self.nightmarephaseindicator:SetScale(scale)

        if visibly_have_medallion then
            self.nightmarephaseindicator:RunObserverInventory()
        end
    end
end

AddClassPostConstruct("screens/playerhud", AddNightmarePhaseIndicator)

